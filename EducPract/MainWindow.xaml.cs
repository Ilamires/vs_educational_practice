﻿using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static EducPract.AVL_Tree;
using static EducPract.Binary_search_tree;
using System.IO;

namespace EducPract
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Student studentTemp;
        private string? fileTemp;
        private List<Student> studentsGoodList;
        private List<Student> studentsAlphabetList;
        private List<Student> studentsBestList;
        private AVL_Tree studentsAVLTree;
        private Binary_search_tree studentsBinarySearchTree;
        public MainWindow()
        {
            InitializeComponent();
            fileTemp = null;
            studentTemp = new Student("", "", new List<int>());
            studentsGoodList = new List<Student>();
            studentsAlphabetList = new List<Student>();
            studentsBestList = new List<Student>();
            studentsAVLTree = new AVL_Tree();
            studentsBinarySearchTree = new Binary_search_tree();
            ErrorLabel.Visibility = Visibility.Hidden;

        }

        private void LoadFileButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            fileTemp = FileNameTextBox.Text;
        }

        private void ClearFileButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            FileNameTextBox.Clear();
        }

        private void LoadNameButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            try
            {
                if (StudentNameTextBox.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                    throw new Exception("Имя не может быть пустым");
                if (StudentNameTextBox.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length > 2)
                    throw new Exception("Слишком много слов");
                studentTemp.name = StudentNameTextBox.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
                studentTemp.surname = StudentNameTextBox.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0];
            }
            catch (Exception ex)
            {
                if (ex is IndexOutOfRangeException)
                    ErrorLabel.Content = "Имя не введено";
                else
                    ErrorLabel.Content = "Некорректные\nимя и фамилия";
                ErrorLabel.Visibility = Visibility.Visible;
            }
        }

        private void ClearNameButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            StudentNameTextBox.Clear();
        }

        private void LoadMarksButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            studentTemp.marks.Clear();
            try
            {
                foreach (string mark in StudentMarksTextBox.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    int markValue = int.Parse(mark);
                    if (2 <= markValue && markValue <= 5)
                        studentTemp.marks.Add(int.Parse(mark));
                    else
                        throw new Exception("ImpossibleMarkValue");
                }
            }
            catch
            {
                studentTemp.marks.Clear();
                ErrorLabel.Content = "Оценками должны быть\n      числа от 2 до 5,\nразделённые пробелами";
                ErrorLabel.Visibility = Visibility.Visible;
            }
        }

        private void ClearMarksButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            StudentMarksTextBox.Clear();
        }

        private void AddStudentsButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            try
            {
                if (fileTemp == null) throw new Exception("Необходимо ввести файл");
                if (fileTemp == "") throw new Exception("Имя файла не может\n" +
                    "        быть пустым");
                if (studentTemp.name == "" || studentTemp.surname == "")
                    throw new Exception("Имя и фамилия не могут\n" +
                        "         быть пустыми");
                if (!studentTemp.marks.Any())
                    throw new Exception("У студента должны\n" +
                        "      быть оценки");
                FileInfo file = new FileInfo(fileTemp + ".txt");
                using (StreamWriter sw = new StreamWriter(fileTemp + ".txt", file.Exists))
                {
                    sw.WriteLine(studentTemp.get_info());
                }
            }
            catch (Exception ex)
            {
                ErrorLabel.Content = ex.Message;
                ErrorLabel.Visibility = Visibility.Visible;
            }
        }

        private void CloseFileButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            fileTemp = null;
        }

        private void PrintList(List<Student> students)
        {
            StructureOutTextBox.Clear();
            foreach (Student student in students)
            {
                StructureOutTextBox.AppendText(student.get_info() + "\n");
            }
        }

        private void GoodAtStartStudentsButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            studentsGoodList.Clear();
            if (fileTemp != null && File.Exists(fileTemp + ".txt"))
                using (StreamReader reader = File.OpenText(fileTemp + ".txt"))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string surname = parts[0];
                        string name = parts[1];
                        List<int> marks = new List<int>();

                        foreach (string mark in parts.Skip(2))
                        {
                            marks.Add(int.Parse(mark));
                        }
                        if (marks.All(x => x == 4))
                        {
                            int i = 0;
                            while (i < studentsGoodList.Count && studentsGoodList[i].marks.All(x => x == 4))
                                ++i;
                            studentsGoodList.Insert(i, new Student(name, surname, marks));
                        }
                        else
                            studentsGoodList.Add(new Student(name, surname, marks));
                    }
                }
            if (studentsGoodList.Any())
            {
                PrintList(studentsGoodList);
                TreeOut.Opacity = 0;
            }
        }

        private void AlphabetStudentsButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            studentsAlphabetList.Clear();
            if (fileTemp != null && File.Exists(fileTemp + ".txt"))
                using (StreamReader reader = File.OpenText(fileTemp + ".txt"))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string surname = parts[0];
                        string name = parts[1];
                        List<int> marks = new List<int>();

                        foreach (string mark in parts.Skip(2))
                        {
                            marks.Add(int.Parse(mark));
                        }
                        int i = 0;
                        while (i < studentsAlphabetList.Count && string.Compare(studentsAlphabetList[i].surname, surname) > 0)
                            ++i;
                        studentsAlphabetList.Insert(i, new Student(name, surname, marks));
                    }
                }
            if (studentsAlphabetList.Any())
            {
                PrintList(studentsAlphabetList);
                TreeOut.Opacity = 0;
            }
        }

        private void SearchTreeStudentsButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            StructureOutTextBox.Clear();
            studentsBinarySearchTree.clear();
            if (fileTemp != null && File.Exists(fileTemp + ".txt"))
                using (StreamReader reader = File.OpenText(fileTemp + ".txt"))
                {
                    string line;
                    line = reader.ReadLine();
                    string[] parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    string surname = parts[0];
                    string name = parts[1];
                    List<int> marks = new List<int>();

                    foreach (string mark in parts.Skip(2))
                    {
                        marks.Add(int.Parse(mark));
                    }
                    studentsBinarySearchTree = new Binary_search_tree(new Binary_search_tree.NodeB(new Student(name, surname, marks)));
                    while ((line = reader.ReadLine()) != null)
                    {
                        parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        surname = parts[0];
                        name = parts[1];
                        marks = new List<int>();

                        foreach (string mark in parts.Skip(2))
                        {
                            marks.Add(int.Parse(mark));
                        }
                        studentsBinarySearchTree.insert(new Student(name, surname, marks));
                    }
                }

            if (studentsBinarySearchTree == null || studentsBinarySearchTree.isEmpty()) return;
            TreeOut.Opacity = 1;
            print3levelsTree(studentsBinarySearchTree);
            //printTree(studentsBinarySearchTree.root);
        }

        private void print3levelsTree(AVL_Tree tree)
        {
            print3levelsTree_recursion(tree.root);
        }
        private void print3levelsTree(Binary_search_tree tree)
        {
            print3levelsTree_recursion(tree.root);
        }
        private void print3levelsTree_recursion(Node? node, int level = 1, string prefix = "", bool root = true, bool last = true)
        {
            if (level > 3) return;

            string ch_right_sep = "\\-", ch_left_sep = "|-", ch_space = "| ";

            switch (level)
            {
                case 1:
                    TreeTextbox1.Text = node != null ? node.student.get_info() : "";
                    break;
                case 2:
                    if (last)
                        TreeTextbox3.Text = node != null ? node.student.get_info() : "";
                    else
                        TreeTextbox2.Text = node != null ? node.student.get_info() : "";
                    break;
                case 3:
                    if (prefix[0] == ' ')
                    {
                        if (last)
                            TreeTextbox7.Text = node != null ? node.student.get_info() : "";
                        else
                            TreeTextbox6.Text = node != null ? node.student.get_info() : "";
                    }
                    else
                    {
                        if (last)
                            TreeTextbox5.Text = node != null ? node.student.get_info() : "";
                        else
                            TreeTextbox4.Text = node != null ? node.student.get_info() : "";
                    }
                    break;
            }
            StructureOutTextBox.AppendText(prefix + (root ? "" : (last ? ch_right_sep : ch_left_sep)) +
                (node != null ? node.student.get_info() : "") + "\n");

            if (node == null || (node.left == null && node.right == null))
                return;

            print3levelsTree_recursion(node.left, level + 1, prefix + (root ? "" : (last ? "  " : ch_space)), false, false);
            print3levelsTree_recursion(node.right, level + 1, prefix + (root ? "" : (last ? "  " : ch_space)), false, true);
        }
        private void print3levelsTree_recursion(NodeB? node, int level = 1, string prefix = "", bool root = true, bool last = true)
        {
            if (level > 3) return;

            string ch_right_sep = "\\-", ch_left_sep = "|-", ch_space = "| ";

            switch (level)
            {
                case 1:
                    TreeTextbox1.Text = node != null ? node.student.get_info() : "";
                    break;
                case 2:
                    if (last)
                        TreeTextbox3.Text = node != null ? node.student.get_info() : "";
                    else
                        TreeTextbox2.Text = node != null ? node.student.get_info() : "";
                    break;
                case 3:
                    if (prefix[0] == ' ')
                    {
                        if (last)
                            TreeTextbox7.Text = node != null ? node.student.get_info() : "";
                        else
                            TreeTextbox6.Text = node != null ? node.student.get_info() : "";
                    }
                    else
                    {
                        if (last)
                            TreeTextbox5.Text = node != null ? node.student.get_info() : "";
                        else
                            TreeTextbox4.Text = node != null ? node.student.get_info() : "";
                    }
                    break;
            }
            StructureOutTextBox.AppendText(prefix + (root ? "" : (last ? ch_right_sep : ch_left_sep)) +
                (node != null ? node.student.get_info() : "") + "\n");

            if (node == null || (node.left == null && node.right == null))
                return;

            print3levelsTree_recursion(node.left, level + 1, prefix + (root ? "" : (last ? "  " : ch_space)), false, false);
            print3levelsTree_recursion(node.right, level + 1, prefix + (root ? "" : (last ? "  " : ch_space)), false, true);
        }

        //private void printTree(Node node)
        //{
        //    if (node == null) return;
        //    printTree(node.left);
        //    StructureOutTextBox.AppendText(node.student.ToString() + " ");
        //    printTree(node.right);
        //}
        //private void printTree(NodeB node)
        //{
        //    if (node == null) return;
        //    printTree(node.left);
        //    StructureOutTextBox.AppendText(node.student.ToString() + " ");
        //    printTree(node.right);
        //}

        private void AVLTreeStudentsButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            StructureOutTextBox.Clear();
            studentsAVLTree.clear();
            if (fileTemp != null && File.Exists(fileTemp + ".txt"))
                using (StreamReader reader = File.OpenText(fileTemp + ".txt"))
                {
                    string line;
                    line = reader.ReadLine();
                    string[] parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    string surname = parts[0];
                    string name = parts[1];
                    List<int> marks = new List<int>();

                    foreach (string mark in parts.Skip(2))
                    {
                        marks.Add(int.Parse(mark));
                    }
                    studentsAVLTree = new AVL_Tree(new AVL_Tree.Node(new Student(name, surname, marks)));
                    while ((line = reader.ReadLine()) != null)
                    {
                        parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        surname = parts[0];
                        name = parts[1];
                        marks = new List<int>();

                        foreach (string mark in parts.Skip(2))
                        {
                            marks.Add(int.Parse(mark));
                        }
                        studentsAVLTree.insert(new Student(name, surname, marks));
                    }
                }
            if (studentsAVLTree == null || studentsAVLTree.isEmpty()) return;
            TreeOut.Opacity = 1;
            print3levelsTree(studentsAVLTree);
            //printTree(studentsAVLTree.root);
        }

        private void PrintBestList(List<Student> students)
        {
            BestOfTheBestTextBox.Clear();
            foreach (Student student in students)
            {
                BestOfTheBestTextBox.AppendText("{" + student.get_info() + "}   ");
            }
        }
        private void GiveBestButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            studentsBestList.Clear();
            if (fileTemp != null && File.Exists(fileTemp + ".txt"))
                using (StreamReader reader = File.OpenText(fileTemp + ".txt"))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string surname = parts[0];
                        string name = parts[1];
                        List<int> marks = new List<int>();

                        foreach (string mark in parts.Skip(2))
                        {
                            marks.Add(int.Parse(mark));
                        }
                        studentsBestList.Add(new Student(name, surname, marks));
                    }
                }
            studentsBestList.Sort((x, y) => y.CompareTo(x));
            PrintBestList(studentsBestList);
        }
    }
}
