﻿using static EducPract.Binary_search_tree;

namespace EducPract
{

    internal class AVL_Tree
    {
        public class Node
        {
            public Student student;
            public Node(Student stud) { student = stud; }

            public int height = 0;
            public Node? left = null;
            public Node? right = null;
        }

        public Node? root;
        public AVL_Tree() { root = null; }
        public AVL_Tree(Node _root) { root = _root; }
        public AVL_Tree(Student stud) { root = new Node(stud); }

        public bool isEmpty() { return root == null; }

        private Node? getMax(Node? node)
        {
            if (node == null) { return null; }
            else if (node.right == null) { return node; }
            else { return getMax(node.right); }
        }

        private int getHeight(Node? node)
        {
            return node == null ? -1 : node.height;
        }

        private void updateHeight(Node? node)
        {
            node.height = Math.Max(getHeight(node.left), getHeight(node.right)) + 1;
        }

        private int getBalance(Node? node)
        {
            return node == null ? 0 : getHeight(node.right) - getHeight(node.left);
        }

        private void swap(Node node1, Node node2)
        {
            Student tmp = node1.student;
            node1.student = node2.student;
            node2.student = tmp;
        }

        private void leftRotate(Node node)
        {
            swap(node, node.right);

            Node tmp = node.left;
            node.left = node.right;
            node.right = node.left.right;

            node.left.right = node.left.left;
            node.left.left = tmp;
            updateHeight(node.left);
            updateHeight(node);
        }

        private void rightRotate(Node node)
        {
            swap(node, node.left);

            Node tmp = node.right;
            node.right = node.left;
            node.left = node.right.left;

            node.right.left = node.right.right;
            node.right.right = tmp;
            updateHeight(node.right);
            updateHeight(node);
        }

        private void balance(Node node)
        {
            int balance = getBalance(node);

            if (balance == -2)
            {
                if (getBalance(node.left) == 1) leftRotate(node.left);
                rightRotate(node);
            }
            else if (balance == 2)
            {
                if (getBalance(node.right) == -1) rightRotate(node.right);
                leftRotate(node);
            }
        }

        private Node find_recursion(Student key, Node node)
        {
            if (node.student.CompareTo(key) == 0 || node == null) return node;
            else if (node.student.CompareTo(key) > 0) return find_recursion(key, node.left);
            else return find_recursion(key, node.right);
        }

        /// <summary>
        /// Найти узел с ключём key
        /// </summary>
        /// <param name="key">Данные для поиска</param>
        /// <returns>Искомый узел</returns>
        public Student find(Student key)
        {
            if (root == null) return null;

            return (find_recursion(key, root)).student;
        }

        private void insert_recursion(Node node, Student key)
        {
            if (node.student.CompareTo(key) <= 0)
            {
                if (node.right == null) { node.right = new Node(key); }
                else { insert_recursion(node.right, key); }
            }
            else if (node.student.CompareTo(key) > 0)
            {
                if (node.left == null) { node.left = new Node(key); }
                else { insert_recursion(node.left, key); }
            }

            updateHeight(node);
            balance(node);
        }

        /// <summary>
        /// Вставить узел с ключём key
        /// </summary>
        /// <param name="key">Ключ искомого узла</param>
        public void insert(Student key)
        {
            if (key == null) { return; }
            if (root == null)
            {
                root = new Node(key);
            }
            else
            {
                insert_recursion(root, key);
            }

        }

        private Node delete_recursion(Student key, Node node)
        {
            if (node == null) { return null; }
            else if (node.student.CompareTo(key) < 0) { node.right = delete_recursion(key, node.right); }
            else if (node.student.CompareTo(key) > 0) { node.left = delete_recursion(key, node.left); }
            else
            {
                if (node.left == null || node.right == null)
                    node = node.left == null ? node.left : node.right;
                else
                {
                    Node maxInLeft = getMax(node.right);
                    node.student = maxInLeft.student;
                    node.right = delete_recursion(maxInLeft.student, node.right);
                }
            }
            if (node != null)
            {
                updateHeight(node);
                balance(node);
            }

            return node;

        }

        /// <summary>
        /// Удалить узел с ключём key
        /// </summary>
        /// <param name="key">Ключ искомого узла</param>
        public void delete(Student key)
        {
            if (key == null || root == null) { return; }
            delete_recursion(key, root);
        }

        private void clear_recursion(ref Node? node)
        {
            if (node == null) { return; };

            clear_recursion(ref node.left);
            clear_recursion(ref node.right);
            node = null;
        }

        public void clear()
        {
            if (root == null) return;

            clear_recursion(ref root);
        }
    }
}
