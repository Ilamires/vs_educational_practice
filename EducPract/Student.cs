﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducPract
{
    public class Student : IComparable<Student>
    {
        public string surname;
        public string name;
        public List<int> marks;

        public Student(string _name, string _surname, List<int> _marks)
        {
            name = _name;
            surname = _surname;
            marks = _marks;
        }

        public double get_avgMark() { return marks.Average(); }
        public string get_info() { return $"{surname} {name} {string.Join(" ", marks.ConvertAll(i => i.ToString()))}"; }

        //public override string ToString()
        //{
        //    return surname + get_avgMark();
        //}

        public int CompareTo(Student? other)
        {
            if (other == null) return -1;
            else return get_avgMark().CompareTo(other.get_avgMark());
        }
    }
}
