﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducPract
{
    internal class Binary_search_tree
    {
        public class NodeB
        {
            public Student student;
            public NodeB(Student stud) { student = stud; }

            public NodeB? left = null;
            public NodeB? right = null;
        }

        public NodeB? root;
        public Binary_search_tree() { root = null; }
        public Binary_search_tree(NodeB _root) { root = _root; }
        public Binary_search_tree(Student stud) { root = new NodeB(stud); }

        public bool isEmpty() { return root == null; }

        private NodeB find_recursion(Student key, NodeB node)
        {
            if (node.student.CompareTo(key) == 0 || node == null) return node;
            else if (node.student.CompareTo(key) > 0) return find_recursion(key, node.left);
            else return find_recursion(key, node.right);
        }

        /// <summary>
        /// Найти узел с ключём key
        /// </summary>
        /// <param name="key">Данные для поиска</param>
        /// <returns>Искомый узел</returns>
        public Student find(Student key)
        {
            if (root == null) return null;

            return (find_recursion(key, root)).student;
        }

        private void insert_recursion(NodeB node, Student key)
        {
            if (node.student.CompareTo(key) <= 0)
            {
                if (node.right == null) { node.right = new NodeB(key); }
                else { insert_recursion(node.right, key); }
            }
            else if (node.student.CompareTo(key) > 0)
            {
                if (node.left == null) { node.left = new NodeB(key); }
                else { insert_recursion(node.left, key); }
            }
        }

        /// <summary>
        /// Вставить узел с ключём key
        /// </summary>
        /// <param name="key">Ключ искомого узла</param>
        public void insert(Student key)
        {
            if (key == null) { return; }
            if (root == null)
            {
                root = new NodeB(key);
            }
            else
            {
                insert_recursion(root, key);
            }

        }

        private Student getMin(NodeB node)
        {
            if (node.left != null) return getMin(node.left);
            else return node.student;
        }

        private NodeB delete_recursion(Student key, NodeB? node)
        {
            if (node == null) return null;
            else if (node.student.CompareTo(key) > 0) node.left = delete_recursion(key, node.left);
            else if (node.student.CompareTo(key) < 0) node.right = delete_recursion(key, node.right);
            else
            {
                if (node.left == null || node.right == null)
                    node = (node.left == null) ? node.right : node.left;
                else
                {
                    Student minKey = getMin(node.left);
                    node.student = minKey;
                    node.left = delete_recursion(minKey, node.left);
                }

            }
            
            return node;
        }

        /// <summary>
        /// Удалить узел с ключём key
        /// </summary>
        /// <param name="key">Ключ искомого узла</param>
        public void delete(Student key)
        {
            if (key == null || root == null) { return; }
            root = delete_recursion(key, root);
        }

        private void clear_recursion(ref NodeB? node)
        {
            if (node == null) { return; };

            clear_recursion(ref node.left);
            clear_recursion(ref node.right);
            node = null;
        }

        public void clear()
        {
            if (root == null) return;

            clear_recursion(ref root);
        }
    }
}
